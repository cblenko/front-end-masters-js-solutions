"use strict";

function identity(o) {
    return o;
}

function add(a, b) {
    return a + b;
}

function mul(a, b) {
    return a * b;
}

function identityf(o) {
    return function() {
        return o;
    }
}

function addf(a) {
    return function(b) {
        return add(a, b);
    }
}

function applyf(fn) {
    return function(a) {
        return function(b) {
            return fn(a, b);
        }
    }
}

function curry(fn, a) {
    return function(b) {
        return fn(a, b);
    }
}

var inc = addf(1);
var inc1 = applyf(add)(1);
var inc2 = curry(add, 1);

function methodise(fn) {
    return function(a) {
        return fn(this, a);
    }
}

Number.prototype.add = methodise(add);
Number.prototype.mul = methodise(mul);

function demethodise(fn) {
    return function(a, b) {
        return fn.call(a, b);
    }
}


function twice(fn) {
    return function(a) {
        return fn(a, a);
    }
}

var double = twice(add);
var square = twice(mul);

function composeu(fnA, fnB) {
    return function(a) {
        return fnB(fnA(a));
    }
}

function composeb(fnA, fnB) {
    return function (a, b, c) {
        return fnB(fnA(a, b), c);
    }
}

function once(fn) {
    var called = false;

    return function(a, b) {
        if(called)
            throw "Already called"

        called = true;

        return fn(a, b);
    }
}

// this is the solution provided by Doug Crockford - but it doesn't work. Nulling fn also nulls f.
function onceA(fn) {
    return function() {
        var f = fn;
        fn = null;

        return f.apply(this, arguments);
    }
}

var addOnce = once(add);
//    console.log(addOnce(3,4));
//    console.log(addOnce(3,4));  // throws

function counterf(v) {
    return {
        inc: function() {
            return ++v;
        },
        dec: function() {
            return --v;
        }
    }
}

var c = counterf(10);
//  console.log(c.inc());
//  console.log(c.dec());

function revocable(fn) {
    var revoked = false;

    return {
        invoke: function() {
            if(revoked)
                throw "function revoked";
    
            return fn.apply(this, arguments)
        },
        revoke: function() {
            revoked = true;
        },
        enable: function() {
            revoked = false;
        }
    } 
}

var addR = revocable(add);
//  console.log(addR.invoke(2,3));
//  console.log(addR.invoke(2,3));
addR.revoke();
//  console.log(addR.invoke(2,3));